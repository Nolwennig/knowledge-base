# Run command in background

src: https://www.maketecheasier.com/run-bash-commands-background-linux/

```
#!bash
command &>/dev/null &
```

# Retourne les fichiers modifiés il y a moins de 4 jours : #

src: http://forum.ubuntu-fr.org/viewtopic.php?pid=15622111#p15622111

```
#!bash
find /path/to/files* -regex '\./[^.].+' -mtime -4
```

src: http://www.linuxcertif.com/man/1/find/

```
#!bash
find [chemin...] [expression] 

* -regex motif
    *     Nom de fichier correspondant à l'expression rationnelle motif. Il s'agit d'une correspondance sur le nom de fichier complet, pas d'une recherche. Par exemple, pour mettre en correspondance un fichier nommé `./fubar3`, vous pouvez utiliser les expressions rationnelles `.*bar.' ou '.*b.*3', mais pas 'b.*r3'.   
* -mtime n
    *     Fichier dont les données ont été modifiées il y a n*24 heures.
```

# Supprime les fichiers modifiés il y a plus de 4 jours #

(retourne et supprime en fait)

```
#!bash
find /path/to/files* -mtime +5 -exec rm {} \;
```

# Retourne l'ip publique #

src: https://openclassrooms.com/forum/sujet/trouver-son-ip-publique-en-console-52837#message-5232323 ==> thx to delta01

```
#!bash
wget http://checkip.dyndns.org -O - -o /dev/null 
# return:<html><head><title>Current IP Check</title></head><body>Current IP Address: 17.5.19.85</body></html>
wget http://checkip.dyndns.org -O - -o /dev/null | cut -d : -f 2 
# return: 17.5.19.85</body></html>
wget http://checkip.dyndns.org -O - -o /dev/null | cut -d : -f 2 | cut -d \< -f 1 
# return: 17.5.19.85
wget http://checkip.dyndns.org -O - -o /dev/null | cut -d : -f 2 | cut -d \< -f 1 | tr -d " " 
# return:17.5.19.85
```

# Ajoute 1G dans /home #

```
#!bash
pvscan
df -h
lvresize -L +1G /dev/mapper/debian--nono--vg-home
resize2fs /dev/mapper/debian--nono--vg-home
```

# l'équivalent de la commande tree de windows (commande apparue avec windows xp) #

srcs: https://forum.ubuntu-fr.org/viewtopic.php?id=86211 , http://www.linux-france.org/lug/gulliver/ml-archives/juin-2004/msg00057.html

```
#!bash
find . | sed 's/[^/]*\//|   /g;s/| *\([^| ]\)/+--- \1/'
```

# afficher le nombre de lignes d'un fichier

src: https://www.developpez.net/forums/d32339/general-developpement/programmation-systeme/linux/nombre-lignes-d-fichier/

```
#!bash
wc -l chemin/vers/le/fichier.txt
```