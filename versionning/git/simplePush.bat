@echo off
title simplePush 0.1
cd %1
git status
echo ------------------
echo.
Set /p commitMsg="Commit message ? "
echo %commitMsg%
color 0A
echo.
echo ###########
echo # Merci ! #
echo ###########
timeout /t 1
color 0F
git add --all
color 03
echo.
echo ##################################
echo # Ajout de fichiers dans l'index #
echo ##################################
color 0F
git commit -m " %commitMsg% "
color 03
echo.
echo ######################################
echo # Validation des fichiers de l'index #
echo ######################################
color 0C
echo.
echo # ... 3 sec pour annuler juste avant le push #
timeout /t 3
color 0F
git push
color 0A
echo.
echo  ###  #  #  ##  #  #     ##  #  #    #
echo  #  # #  # #  # #  #    #  # # #     #
echo  #  # #  #  #   ####    #  # ##      #
echo  ###  #  #   #  #  #    #  # ##      #
echo  #    #  # #  # #  #    #  # # #
echo  #     ##   ##  #  #     ##  #  #    #
echo.
color 0F
git status
color 0A
pause