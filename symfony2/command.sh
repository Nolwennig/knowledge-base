# Ajouter ces lignes Ã  la fin de votre ~/.bashrc ou ~/.zshrc selon le terminal que vous utilisez
# taper source ~/.zshrc si vous Ãªtes sur zsh pour appliquer les modifs

#######################
#   SYMFONY ALIASES   #
#######################

alias sf='php app/console'

### clear cache
alias sfcc='php app/console cache:clear'

### assets install (fyi vous pouvez taper sfassets --symlink)
alias sfassets='php app/console assets:install'

### assetic dump
alias asseticd='php app/console assetic:dump'

### assetic watch
alias asseticw='php app/console assetic:watch'

### generate bundle
alias sfgenmod='php app/console generate:bundle'

### generate controller
alias sfgencon='php app/console generate:controller'

### generate form
alias sfform='php app/console generate:doctrine:form'

### generate entity
alias sfent='php app/console doctrine:generate:entity'

### generate entity's functions
alias sfents='php app/console doctrine:generate:entities'

### db migrations migrate
alias sfmig='php app/console doctrine:migrations:migrate --no-interaction'

### db migrations exec
alias sfmigex='php app/console doctrine:migrations:exec'

### reset database
alias sfrdb='php app/console doctrine:database:drop --force && php app/console doctrine:database:create && php app/console doctrine:migrations:migrate --no-interaction && php app/console doctrine:fixtures:load --no-interaction'