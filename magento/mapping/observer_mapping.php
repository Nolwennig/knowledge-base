<?php

/**
 * 2015-04-24
 * Nolwennig
 * Adapted from http://www.jyotiranjan.in/blog/how-to-find-all-rewrite-classes-in-magento/
 * Usage :
 * - Go to this directory.
 * - Run 'php observer_mapping.php > observer_mapping.txt'.
 * - Download observer_mapping.txt.
 * - Read, learn & enjoy!
 */
$folders     = array('../www/app/code/local/', '../www/app/code/community/'); //folders to parse
$configFiles = array();
foreach ($folders as $folder)
{
    $files       = glob($folder . '*/*/etc/config.xml'); //get all config.xml files in the specified folder
    $configFiles = array_merge($configFiles, $files); //merge with the rest of the config files
}
$observers = array(); //list of all observers

foreach ($configFiles as $file)
{
    $dom                 = new DOMDocument;
    $dom->loadXML(file_get_contents($file));
    $xpath               = new DOMXPath($dom);
    $path                = '//events/*'; //search for tags named 'events' 
    // le module Iota_Events remonte des infos erronées dans le dump
    $iotaEventsExclusion = array('args', 'class', 'file', 'resourceModel', 'use'); // dirty but works fine
    $text                = $xpath->query($path);
    foreach ($text as $observerElement)
    {
        $name = $observerElement->tagName;
        if (!in_array($name, $iotaEventsExclusion)) {
            foreach ($observerElement->childNodes as $element)
            {
                $content = trim($element->textContent);
                if ($content != null) {
                    $observers[$name][] = $content; //observer config
                }
            }
        }
    }
}
ksort($observers);
print_r($observers);
?>