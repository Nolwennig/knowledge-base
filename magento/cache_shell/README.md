1. put the cache.php file into <magento_root_dir>/shell/ dir
2. then run php -f shell/cache.php

Usage:  php -f cache.php -- [options]
  info                          Show Magento cache types.
  enable <cachetype>            Enable caching for a cachetype.
  disable <cachetype>           Disable caching for a cachetype.
  refresh <cachetype>           Clean cache types.
  flush <magento|storage>       Flushes slow|fast cache storage.

  cleanmedia                    Clean the JS/CSS cache.
  cleanimages                   Clean the image cache.
  destroy                       Clear all caches.
  help                          This help.

  <cachetype>     Comma separated cache codes or value "all" for all caches