<?php

/**
 * Ce script supprime les produits associÃ©s a un store
 */
$project_path = "/home/www/edilivre/dev/www";   // ici qualif patrimoine
$store_id = 8;                                  // ici store bnf
$limit = 10000;                                 // traitement par lot de 10mill produits
$page = 1;                                      // pagination
$paginate = TRUE;                               // boucle de pagination
// Pour garder une console propre
error_reporting(~E_WARNING & ~E_NOTICE); // Rapporte toutes les erreurs Ã  part les E_NOTICE et les E_WARNING
ini_set('memory_limit', '2G');
ob_end_flush();
umask(0);
require_once $project_path . '/app/Mage.php';

echo '---' . "\n" . "\t" . "Debut : " . date("d/m/y h:i:s") . "\n";
Mage::register('isSecureArea', true);
Mage::app()->setCurrentStore($store_id);
echo "\t" . 'Store : ' . Mage::app()->getStore()->getName() . ' [' . Mage::app()->getStore()->getId() . ']' . "\n";

while ($paginate) {
    $products = Mage::getModel('catalog/product')->getCollection()
            ->addStoreFilter($store_id)
            ->setPageSize($limit)
            ->setCurPage(1);

    $total_products = count($products);

    if ($total_products == 0) {
        $paginate = FALSE;
    } else {
        $sql = "";
        $undoSql = "";
        for ($i = 0; $i <= 8; $i++) {
            $sql .= "UPDATE index_process SET mode = 'manual' WHERE index_process.process_id =$i LIMIT 1;";
            $undoSql .= "UPDATE index_process SET mode = 'real_time' WHERE index_process.process_id =$i LIMIT 1;";
        }

        $mysqli = Mage::getSingleton('core/resource')->getConnection('core_write');
        $mysqli->query($sql);

        $count = 0;
        echo "\t" . 'Lot ' . $page . ' = ' . $total_products . ' produits recupere en DB' . "\n";

        foreach ($products as $product) {
            $product->delete();
            echo '#';
            $count++;
            if ((round((($count / $total_products) * 100), 0) > 0) && ($count % ($limit / 100) == 0)
            ) {
                echo "\t" . round((($count / $total_products) * 100), 0) . '%' . ' [' . $count . '/' . $total_products . ']' . "\n";
            }
        }
        $page++;
    }
}
Mage::unregister('isSecureArea');
echo "--- FIN de la suppression " . date("d/m/y h:i:s");
$mysqli->query($undoSql);
exit();
?>