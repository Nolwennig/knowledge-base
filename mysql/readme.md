# Afficher taille

## Afficher la taille des bases MySQL (tous les schémas)

 - src : http://www.vincentliefooghe.net/content/calculer-la-taille-dune-base-mysql

```
#!sql
SELECT 
 table_schema DB,
 FLOOR(SUM( data_length + index_length ) /1024 /1024 /1024) G,
 FLOOR(((SUM( data_length + index_length ) /1024 /1024 /1024)-(FLOOR(SUM( data_length + index_length ) /1024 /1024 /1024)))*1024) M,
 FLOOR(((SUM( data_length + index_length ) /1024 /1024 )-(FLOOR(SUM( data_length + index_length ) /1024 /1024 )))*1024) k,
 FLOOR(((SUM( data_length + index_length ) /1024 )-(FLOOR(SUM( data_length + index_length ) /1024)))*1024) o,
 SUM( data_length + index_length ) total
FROM 
 information_schema.tables
GROUP BY 
 table_schema
```

## Afficher les tailles des tables d'une base MySQL (pour un schéma spécifique)

```
#!sql
SELECT table_name, table_rows, data_length, index_length, 
 round(((data_length + index_length) / 1024 / 1024),2) "Taille en Mo"
FROM
 information_schema.tables 
WHERE 
 table_schema = "__DATABASE_NAME__"
ORDER BY 
 round(((data_length + index_length) / 1024 / 1024),2) DESC
```

# Rechercher un nom de colonne dans une db #

```
#!sql
SELECT column_name, table_name FROM information_schema.columns WHERE column_name LIKE '%__COL_QUERY__%' AND table_schema='__DATABASE_NAME__';
```
# Lister colonnes, pk, fk

## Lister toutes les colonnes d'une table

```
#!sql
SELECT column_name FROM information_schema.columns WHERE table_name = '__TABLE_NAME__' AND table_schema='__DATABASE_NAME__';
```

## Lister toutes les colonnes d'une db

```
#!sql
SELECT column_name, table_name FROM information_schema.columns WHERE table_schema='__DATABASE_NAME__';
```

## Lister les clés primaires (PK) d'une db

```
#!sql
select
    concat(table_schema, '.', table_name) as 'primary key',  
from
    information_schema.table_constraints
where
    constraint_type = 'PRIMARY KEY'
    and table_schema = '__DATABASE_NAME__'
```

## Lister les clés étrangères (FK) d'une db

```
#!sql
select
    concat(table_name, '.', column_name) as 'foreign key',  
    concat(referenced_table_name, '.', referenced_column_name) as 'references'
from
    information_schema.key_column_usage
where
    referenced_table_name is not null
    and table_schema = '__DATABASE_NAME__'
```

# Diff table, database

## Faire le différentiel rapide entre 2 db (Structure only)

src :

 - http://stackoverflow.com/a/8718572/4457531
 - http://stackoverflow.com/a/26328331/4457531

```
#!sh
mysqldump --no-data --skip-comments --skip-extended-insert -h __DB_HOSTNAME__ -u __DB_USERNAME__ -p __DB1_NAME__ | sed 's/ AUTO_INCREMENT=[0-9]*//g' > FILENAME_1.sql
mysqldump --no-data --skip-comments --skip-extended-insert -h __DB_HOSTNAME__ -u __DB_USERNAME__ -p __DB2_NAME__ | sed 's/ AUTO_INCREMENT=[0-9]*//g' > FILENAME_2.sql
diff FILENAME_1.sql FILENAME_2.sql > DIFF_FILENAME.txt
cat DIFF_FILENAME.txt | less
```

## Diff entre 2 tables (structure identique)
```
#!sql
SELECT * FROM db1.T1
WHERE db1.T1.ID NOT IN (SELECT db2.T2.ID FROM db2.T2)
```


# Fusionner 2 tables (structure identique)
	
Use phpMyAdmin > Go to the current database > click import and import the other database

To insert all rows of a table in s1 into a table in s2, while overwriting existing lines, you can use:
```
#!sql
REPLACE INTO s2.table_name
SELECT * FROM s1.table_name;
```

Don't want to touch existing lines:
```
#!sql
INSERT INTO s2.table_name
SELECT * FROM s1.table_name
ON DUPLICATE KEY IGNORE;
```


# Renommer une database

 - src : http://www.rndblog.com/how-to-rename-a-database-in-mysql/

```
#!sh
mysqldump -uxxxx -pxxxx -h xxxx db_name > db_name_dump.sql
mysql -uxxxx -pxxxx -h xxxx -e "CREATE DATABASE new_db_name"
mysql -uxxxx -pxxxx -h xxxx new_db_name < db_name_dump.sql
mysql -uxxxx -pxxxx -h xxxx -e "DROP DATABASE db_name"
```

# Dump tables

## Dump few tables

 - src: https://dba.stackexchange.com/a/9309/127895

```
#!sh
mysqldump -u... -p... mydb t1 t2 > mydb_only-t1-t2.sql
```

## Dump all table without few tables

 - src: https://dba.stackexchange.com/a/30480/127895

```
#!sh
mysqldump -u... -p... mydb --ignore-table=mydb.t1 --ignore-table=mydb.t2 > mydb_without-t1-t2.sql
```
