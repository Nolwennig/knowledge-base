## Get the query executed in Laravel 4

Call DB::getQueryLog() to get all ran queries.

```
#!php
$queries = DB::getQueryLog();
$last_query = end($queries);
```

## Command Line

Run `php artisan list` for find all the commands available for artisan