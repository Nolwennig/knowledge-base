# README #

## A lire ##

* http://codepen.io/Sonick/pen/HthaI <- showing text with effect
* http://codepen.io/anon/pen/GJqYge <- text shadow
* http://codepen.io/brownerd/pen/rVLjvE <- the 4 grid techniques
* http://codepen.io/tholman/pen/qCnfB <- draw the text
* http://codepen.io/wagerfield/pen/wftcE <- show text with effect
* http://codepen.io/yoannhel/pen/sJpDj <- show text with effect (front rotation)
* http://codepen.io/rachsmith/pen/BNKJme <- show text with effect (front rotation)
* http://codepen.io/jackrugile/pen/xAeHr <- show text with effect
* http://codepen.io/neilcarpenter/pen/LstCD <- show text with effect
* http://codepen.io/neilcarpenter/pen/IqJbi <- show text with effect
* http://codepen.io/theCSSguru/pen/DAidI <- css3 menu gradient
* http://codepen.io/jvlahos/pen/LdynE <- highlight text
* http://codepen.io/jackrugile/pen/aLkxA <- css3 text shadow effect
* http://codepen.io/danielbhansen/pen/pwFsL <- text blur to sharp on hover