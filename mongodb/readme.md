# MongoDB #

## Query ##

### Find last record entry in collection ###
`db.collection.find().limit(1).sort({$natural:-1})`

## Shell Methods ##

### Se connecter ###
`mongo -uUSER -pPASS --host=HOST DB_NAME`

### Dump all collections ###
`mongodump --host HOST --db DB_NAME --username USER --password PASS`

### Dump one collection ###
`mongodump --host HOST --db DB_NAME --username USER --password PASS --collection COLLECTION_NAME`

### Restore DB complete ###
`mongorestore --username USER --password PASS --db DB_NAME --drop PATH`

### Restore one collection ###
`mongorestore --username USER --password PASS --db DB_NAME PATH`